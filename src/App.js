import React, { Component } from 'react';
import './App.css';
import Item from "./Components/Item";
import Order from "./Order/Order";


class App extends Component {
    state = {
        items: [
            {name: 'Hamburger', price: 80, number: 0, id: '1'},
            {name: 'Cheeseburger', price: 90, number: 0, id: '2'},
            {name: 'Fries', price: 45, number: 0, id: '3'},
            {name: 'Coffee', price: 70, number: 0, id: '4'},
            {name: 'Tea', price: 50, number: 0, id: '5'},
            {name: 'Cola', price: 40, number: 0, id: '6'}
        ]

    };
    removeItemFromOrder = (id) => {
        const index = this.state.items.findIndex(p => p.id === id);
        const item = {...this.state.items[index]};
        item.number = 0;

        const items = [...this.state.items];
        items[index] = item;

        this.setState({items});
    };

    increaseNumber = (id) => {
        const index = this.state.items.findIndex(p => p.id === id);
        const item = {...this.state.items[index]};
        item.number++;

        const items = [...this.state.items];
        items[index] = item;

        this.setState({items});

    };

    totalSum = () => {

        let totalSum = 0;
        for (let i = 0; i < this.state.items.length; i++) {
            let sum = this.state.items[i].price * this.state.items[i].number;
            totalSum += sum;
        }
        return totalSum;
    };

    totalNumber = () => {
        return this.state.items.reduce((acc, el) => acc + el.number, 0);
    };

    decreaseItemFromOrder = (id) => {
        const index = this.state.items.findIndex(p => p.id === id);
        const item = {...this.state.items[index]};
        item.number--;

        const items = [...this.state.items];
        items[index] = item;

        this.setState({items});
    };

  render() {

    return (
      <div className="App">+
        <header className="App-header">
          <h1 className="App-title">Welcome to Fast Food Menu</h1>
          <h2 className="App-title">Choose item from the below menu and add it to order list</h2>
        </header>
          <div className='Container'>
              <div className='OrderDetails'>
                  <h2>Order Details</h2>
                  {this.totalNumber() === 0 ? <div className='header'>
                      <p>Order is empty!</p>
                      <p>Please enter some items</p>
                  </div> : null }

                  {this.state.items.map((item) => {
                      if (item.number === 0) {return null}
                      return <Order
                          number={item.number}
                          key={item.id}
                          name={item.name}
                          price={item.price}
                          total={item.number * item.price}
                          remove={() => this.removeItemFromOrder(item.id)}
                          decrease={() => this.decreaseItemFromOrder(item.id)}
                          >
                      </Order>;

                  })}
                  <p>------------------------</p>
                <p>Total Price : {this.totalSum()}</p>

              </div>

          <div className='AddItems'>
              <h2>Add Items</h2>
              {this.state.items.map((item) => {
                  return <Item
                      number={item.number}
                      key={item.id}
                      name={item.name}
                      price={item.price}
                      click={() => this.increaseNumber(item.id)}>
                  </Item>
              })}
          </div>

          </div>
          </div>
    );
  }
}

export default App;
