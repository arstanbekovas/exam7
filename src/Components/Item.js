import React from 'react';
import './Item.css';

const Item = props => {
    return (
        <div className="Item" >
            <h3>{props.name}</h3>
            <p>Price: {props.price} KGS</p>
            <button className='click' onClick={props.click}> Add to order list </button>
        </div>
    );
};

export default Item;