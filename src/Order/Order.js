import React from 'react';
import './Order.css';

const Order = props => {
    return (
        <div className="Order" >
            <p className='name'>{props.name}</p>
            <p className='number'>x {props.number}</p>
            <p className='price'>Price: {props.price} KGS</p>
            <p>Total:{props.total}</p>
            <p className='remove' onClick={props.decrease}>{props.children} -</p>
            <p className='remove' onClick={props.remove}>{props.children} X</p>

        </div>
    );
};

export default Order;